#!/usr/bin/env python3 

import subprocess, sys
import re
# import curses
# from curses import wrapper
from pprint import pprint


def run(command, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE):
   p = subprocess.Popen(command.split(), stdin=stdin, stdout=stdout, stderr=stderr)
   out, err = p.communicate()
   return out, err


def check_efibootmgr():
   try:
      o, e = run("efibootmgr -V")
   except FileNotFoundError:
      return False
   if e:
      return False
   return True


def get_records():
   o, e = run("efibootmgr")
   if e:
      return None

   if o:
      raw_ouput = o.decode()
   else:
      return None

   dat = {"boot_entries":{}}
   for l in raw_ouput.splitlines():
      if "BootCurrent: " in l:
         dat["current_boot"] = l[len("BootCurrent: "):]
      elif "Timeout: " in l:
         dat["timeout"] = l[len("Timeout: "):]
      elif "BootOrder: " in l:
         dat["boot_order"] = list(l[len("BootOrder: "):].split(","))
      else:
         continue
   matches = re.finditer(r"Boot(\d*)\* (.*)", raw_ouput, re.MULTILINE)
   for match in matches:
      dat["boot_entries"][match.group(1)] = match.group(2)

   return dat


def change_ubuntu_first():
   info = get_records()
   ubu = None

   if not info:
      return False
   print("Old order: {}".format(info["boot_order"]))

   for k in info["boot_entries"].keys():
      if "ubuntu" in info["boot_entries"][k].lower():
         ubu = k
         break
   if not ubu:
      return False

   info["boot_order"].remove(ubu)
   new_order = [ubu] + info["boot_order"]

   for r in new_order:
      if r not in info["boot_entries"].keys():
         new_order.remove(r)
         continue
   for k in info["boot_entries"].keys():
      if k not in new_order:
         new_order.append(k)
         continue
   print("New order: {}".format(new_order))

   print("efibootmgr -o {}".format(",".join(new_order)))

def change_numerically():
   info = get_records()
   ubu = None

   if not info:
      return False
   print("Old order: {}".format(info["boot_order"]))

   new_order = list()

   for k in info["boot_entries"].keys():
      new_order.append(k)

   for r in new_order:
      if r not in info["boot_entries"].keys():
         new_order.remove(r)
         continue
   for k in info["boot_entries"].keys():
      if k not in new_order:
         new_order.append(k)
         continue
   print("New order: {}".format(new_order))

   print("efibootmgr -o {}".format(",".join(new_order)))

def main():
   if not check_efibootmgr():
      sys.exit("efibootmgr not installed.")
   pprint(get_records())
   change_ubuntu_first()
   change_numerically()


if __name__ == '__main__':
   # wrapper(main)
   main()
 
